'use strict';

const http = use('App/core/http-service');

class ConfigurationController {
	index({ response, request }) {
		const headers = request.headers();
		return http.getAll('configuration', headers, request.all()).then((posts) => response.send(posts.body));
	}

	about({ response, request }) {
		const headers = request.headers();
		return http.getAll('configuration/about', headers, request.all()).then((posts) => response.send(posts.body));
	}
}

module.exports = ConfigurationController;
