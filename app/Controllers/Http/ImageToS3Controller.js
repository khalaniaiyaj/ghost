 'use strict';
 const AWS = require('aws-sdk');
 const Env = use('Env');
  const Drive = use('Drive');

class ImageToS3Controller {
		async getPreSignedUrl({ params , response}) {
            const filename = params.filename
            var newFileName = Math.random().toString(36).substring(7) + filename
            const signedUrl = await Drive.disk('s3').getSignedUrl(newFileName);
            
            const res = { "url" : signedUrl }
            res.fileName = newFileName
            response.send(res);
	 }
}
module.exports = ImageToS3Controller;
