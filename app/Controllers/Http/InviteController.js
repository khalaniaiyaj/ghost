'use strict';
const http = use('App/core/http-service');

class InviteController {
	post({ request, response }) {
		const headers = request.headers();
		return http.post('invites', headers, request.all()).then((invites) => response.send(invites));
	}

	index({ response, request }) {
		const headers = request.headers();
		return http.getAll('invites', headers, request.all()).then((posts) => response.send(posts.body));
	}

	show({ response, params, request }) {
		const headers = request.headers();
		return http.get('invites', params.id, headers , request.all()).then((res) => response.send(res.body));
	}

	delete({ response, params, request }) {
		const headers = request.headers();
		return http.delete('invites', params.id, headers).then((res) => response.send(res.statusCode));
	}
}

module.exports = InviteController;
