'use strict';

const http = use('App/core/http-service');

class MigrationController {
	post({ response, request }) {
		const file = request.file('importfile');
		return http.uploads('db/', file).then((path) => response.send(path));
	}

	index({ response, request }) {
		const headers = request.headers();
		return http.exportDb('db/', request.all()).then((posts) => response.send(posts.body));
	}

	delete({ response }) {
		return http.deleteWithoutId('db/').then((res) => response.send(res.statusCode));
	}
}

module.exports = MigrationController;
