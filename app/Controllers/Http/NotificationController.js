'use strict';

const http = use('App/core/http-service');


class NotificationController {
    index({ response, request }) {
		const headers = request.headers();		
		return http.getAll('notifications', headers ,request.all()).then((posts) => response.send(posts.body));
	}
}

module.exports = NotificationController
