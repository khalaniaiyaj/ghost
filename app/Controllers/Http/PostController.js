'use strict';

const http = use('App/core/http-service');
const Post = use('App/Models/Post');
const Language = use('App/Models/Language');
const Database = use('Database');
const PostLanguages = use('App/Models/PostLanguage');

class PostController {
	//create
	async post({ response, request }) {
		const headers = request.headers();
		return await http.post(request.body.language, 'posts', headers, request.all()).then(async (posts) => {
			//save in post table
			const post = new Post();
			const language = new Language();
			const postLanguages = new PostLanguages();
			post.title = posts.posts[0].title;
			await post.save();

			//save in pivot table
			postLanguages.post_id = post.id;
			postLanguages.ghost_post_id = posts.posts[0].id;
			postLanguages.language_id = request.body.language_id;
			await postLanguages.save();
			response.send(posts);
		});
	}

	//index
	async index({ response, request }) {
		const headers = request.headers();
		return await Post.query().with('languages').fetch();
	}

	//show
	show({ response, params, request }) {
		const headers = request.headers();
		return http
			.get(request.body.language, 'posts', params.id, headers, request.all())
			.then((posts) => response.send(posts));
	}

	showBySlug({ response, params, request }) {
		const headers = request.headers();
		return http
			.get(request.body.language, 'posts/slug', params.slug, headers, request.all())
			.then((posts) => response.send(posts.body));
	}

	//update
	put({ response, params, request }) {
		const headers = request.headers();
		return http
			.put(request.body.language, 'posts', params.id, headers, request.all())
			.then((posts) => response.send(posts));
	}

	//delete
	delete({ response, params, request }) {
		const headers = request.headers();
		return http.delete(request.body.language, 'posts', params.id, headers).then((res) => response.send(res));
	}
	
	async addNewTranslation({ request, params, response }) {
		const headers = request.headers();
		return await http.post(request.body.language, 'posts', headers, request.all()).then(async (res) => {
			const postId = await Post.find(params.id);
			const posts = await postId.languages().fetch();
			const postLanguages = new PostLanguages();

			//save in pivot table
			postLanguages.post_id = params.id;
			postLanguages.ghost_post_id = res.posts[0].id;
			postLanguages.language_id = request.body.language_id;
			await postLanguages.save();

			return res;
		});
	}
}

module.exports = PostController;
