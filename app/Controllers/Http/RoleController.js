'use strict';

const http = use('App/core/http-service');

class RoleController {
	index({ response, request }) {
		const headers = request.headers();		
		return http.getAll('roles', headers ,request.all()).then((posts) => response.send(posts.body));
	}
}

module.exports = RoleController;
