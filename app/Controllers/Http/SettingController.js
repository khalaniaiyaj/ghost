'use strict';

const http = use('App/core/http-service');

class SettingController {
	index({ response, request }) {
		const headers = request.headers();
		return http.getAll('settings', headers, request.all()).then((res) => response.send(res.body));
	}

	put({ response, request }) {
		const headers = request.headers();
		return http.putWithoutId('settings/', headers, request.all()).then((res) => response.send(res.body));
	}
}

module.exports = SettingController;
