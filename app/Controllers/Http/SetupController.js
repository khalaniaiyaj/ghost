'use strict';

const http = use('App/core/http-service');

class SetupController {
	post({ response, request }) {
		const headers = request.headers();
		return http.post('authentication/setup/', headers).then((invites) => response.send(invites));
	}

	index({ response, request }) {
		const headers = request.headers();
		return http.getAll('authentication/setup/', headers, request.all()).then((posts) => response.send(posts.body));
	}
}

module.exports = SetupController;
