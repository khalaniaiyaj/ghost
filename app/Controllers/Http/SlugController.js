'use strict';
const http = use('App/core/http-service');

class SlugController {
	show({ response, params, request }) {
		const headers = request.headers();
		return http.get('slugs/post', params.slug, headers).then((slugs) => response.send(slugs.body));
	}
}

module.exports = SlugController;
