'use strict';
const http = use('App/core/http-service');

class TagController {
	//create
	post({ response, request }) {
		const headers = request.headers();
		return http.post(request.body.language, 'tags', headers, request.all()).then((posts) => response.send(posts));
	}

	//index
	index({ response, request }) {
		const headers = request.headers();
		return http
			.getAll(request.body.language, 'tags', headers, request.all())
			.then((posts) => response.send(posts));
	}

	//show
	show({ response, params, request }) {
		const headers = request.headers();
		return http
			.get(request.body.language, 'tags/slug', params.id, headers)
			.then((posts) => response.send(posts));
	}

	//update
	put({ response, params, request }) {
		const headers = request.headers();
		return http
			.put(request.body.language, 'tags', params.id, headers, request.all())
			.then((posts) => response.send(posts));
	}

	//delete
	delete({ response, params, request }) {
		const headers = request.headers();
		return http
			.delete(request.body.language, 'tags', params.id, headers)
			.then((res) => response.send(res.statusCode));
	}
}

module.exports = TagController;
