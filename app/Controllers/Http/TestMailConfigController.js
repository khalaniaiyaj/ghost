'use strict';

const http = use('App/core/http-service');

class TestMailConfigController {
	post({ response, request }) {
		const headers = request.headers();
		return http.post('mail/test/', headers).then((posts) => response.send(posts));
	}
}

module.exports = TestMailConfigController;
