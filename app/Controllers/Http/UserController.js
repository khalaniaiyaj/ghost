'use strict';
const http = use('App/core/http-service');
const Database = use('Database');
const _ = require('lodash');

class UserController {
	//user login
	async login({ request }) {
		const headers = request.headers();
		const data = request.body.data;
		const language = {};
		const data2 = {
			grant_type: data.grant_type,
			client_id: data.client_id,
			client_secret: data.client_secret,
			username: request.input('username'),
			password: request.input('password')
		};
		const language_from_database = await Database.select('name')
			.first()
			.from('languages')
			.where('id', request.body.language_id);

		return http.postToken(data.url, data2).then((response) => {
			this.language = {
				language: language_from_database.name
			};
			let res = _.mergeWith({}, response.body, this.language);
			return res;
		});
	}

	//get all user
	index({ response, request }) {
		const headers = request.headers();
		return http
			.getAll(request.body.language, 'users', headers, request.all())
			.then((res) => response.send(res));
	}

	//give current user
	showMe({ response, request }) {
		const headers = request.headers();
		return http
			.get(request.body.language, 'users/me', headers, request.all())
			.then((res) => response.send(res.body));
	}

	//show by slug
	showBySlug({ response, request, params }) {
		const headers = request.headers();
		return http
			.get(request.body.language, 'users/slug', params.slug, headers)
			.then((posts) => response.send(posts.body));
	}

	//get user
	show({ response, request, params }) {
		const headers = request.headers();
		return http
			.get(request.body.language, 'users', params.id, headers, request.all())
			.then((res) => response.send(res));
	}

	//change password
	changePassword({ response, request }) {
		const headers = request.headers();
		return http
			.putWithoutId(request.body.language, 'users/password/', headers, request.all())
			.then((res) => response.send(res.body));
	}

	//edit user
	put({ response, params, request }) {
		const headers = request.headers();
		return http.put('/users', params.id, headers, request.all()).then((posts) => response.send(posts.body));
	}

	//delete user
	delete({ response, params, request }) {
		const headers = request.headers();
		return http
			.delete(request.body.language, 'users', params.id, headers)
			.then((res) => response.send(res.statusCode));
	}

	url({ request }) {
		return request.body;
	}
}

module.exports = UserController;
