'use strict';
const Database = use('Database');
const _ = require('lodash');

class ClientIdSecretdata {
	async handle({ request, response }, next) {
		const data = await Database.from('languages').where('id', request.input('language_id')).first();
		request.body.data = data;
		await next();
	}
}

module.exports = ClientIdSecretdata;
