'use strict';
const Database = use('Database');

class LanguageHeader {
	async handle({ request }, next) {
		const headers = request.headers();
		const data = await Database.select('url', 'id').from('languages').where('name', headers['x-language']);
		request.body.language = data[0].url;
		request.body.language_id = data[0].id;
		await next();
	}
}

module.exports = LanguageHeader;
