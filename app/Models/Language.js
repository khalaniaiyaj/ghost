'use strict'

const Model = use('Model')

class Language extends Model {
    posts() {
        return this.belongsToMany('App/Models/Posts','language_id','post_id').pivotTable('post_languages')
    }
}

module.exports = Language
