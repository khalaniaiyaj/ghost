'use strict';

const Model = use('Model');

class Post extends Model {
	languages() {
		return this.belongsToMany('App/Models/Language','post_id','language_id').pivotTable('post_languages').withTimestamps().withPivot(['ghost_post_id']);
	}
}

module.exports = Post;
