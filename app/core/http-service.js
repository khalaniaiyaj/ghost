const rp = use('request-promise');
var fs = require('fs');
var multer = require('multer');

let httpRequest = {};

httpRequest.postToken = function(url, data) {
	return rp
		.post({
			url: `${url}/authentication/token`,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			form: data,
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => {
			return response;
		});
};

httpRequest.post = function(apiUrl, url, headers, data) {
	return rp
		.post({
			url: `${apiUrl}${url}`,
			headers: {
				Authorization: headers.authorization
			},
			body: data,
			json: true
		})
		.then((response) => response);
};

httpRequest.getAll = function(apiUrl, url, headers, data) {
	return rp
		.get({
			url: `${apiUrl}${url}`,
			headers: {
				Authorization: headers.authorization
			},
			qs: data,
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};

httpRequest.get = function(apiUrl, url, id, headers, data) {
	return rp
		.get({
			url: `${apiUrl}${url}/${id}`,
			headers: {
				Authorization: headers.authorization
			},
			qs: data,
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};

httpRequest.delete = function(apiUrl, url, id, headers) {
	return rp
		.del({
			url: `${apiUrl}${url}/${id}`,
			headers: {
				Authorization: headers.authorization
			},
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};

httpRequest.put = function(apiUrl, url, id, headers, data) {
	return rp
		.put({
			url: `${apiUrl}${url}/${id}`,
			headers: {
				Authorization: headers.authorization
			},
			body: data,
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};

httpRequest.putWithoutId = function(apiUrl, url, headers, data) {
	return rp
		.put({
			url: `${apiUrl}${url}`,
			headers: {
				Authorization: headers.authorization
			},
			body: data,
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};

httpRequest.uploads = function(apiUrl, url, headers, data) {
	filename = data.headers['content-disposition'].split(';')[2].trim().split('=')[1];
	console.log(JSON.parse(filename));
	console.log(apiUrl);
	return rp
		.post({
			url: `${apiUrl}${url}`,
			headers: {
				Authorization: headers.authorization
			},
			formData: {
				file: {
					value: fs.createReadStream('tmp/uploads/' + filename),
					options: {
						contentType: 'image/*'
					}
				}
			}
		})
		.then((response) => response);
};

//for migration
httpRequest.exportDb = function(url, data) {
	return rp
		.get({
			url: `${apiUrl}${url}`,
			qs: data,
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};

httpRequest.deleteWithoutId = function(url) {
	return rp
		.del({
			url: `${apiUrl}${url}`,
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};

module.exports = httpRequest;
