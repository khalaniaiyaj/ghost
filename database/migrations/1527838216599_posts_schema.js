'use strict';

const Schema = use('Schema');

class PostsSchema extends Schema {
	up() {
		this.create('posts', (table) => {
			table.increments();
			table.string('ghost_uuid');
			table.string('title');
			table.timestamps();
		});
	}

	down() {
		this.drop('posts');
	}
}

module.exports = PostsSchema;
