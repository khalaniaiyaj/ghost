'use strict';

const Schema = use('Schema');

class PostLanguagesSchema extends Schema {
	up() {
		this.create('post_languages', (table) => {
			table.increments();
			table.integer('post_id').notNullable();
      table.integer('language_id').notNullable();
     
			table.timestamps();
		});
	}

	down() {
		this.drop('post_languages');
	}
}

module.exports = PostLanguagesSchema;
