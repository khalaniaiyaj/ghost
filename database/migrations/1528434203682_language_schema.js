'use strict';

const Schema = use('Schema');

class LanguageSchema extends Schema {
	up() {
		this.create('languages', (table) => {
			table.increments();
			table.string('name').unique();
			table.string('client_id', 255).notNullable();
			table.string('client_secret', 80).notNullable();
			table.string('url');
			table.string('grant_type').default('password');
		});
	}

	down() {
		this.drop('languages');
	}
}

module.exports = LanguageSchema;
