'use strict'

const Schema = use('Schema')

class PostLanguagesSchema extends Schema {
  up () {
    this.table('post_languages', (table) => {
      // alter table
      table.string('ghost_post_id').unique()
    })
  }

  down () {
    this.table('post_languages', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PostLanguagesSchema
