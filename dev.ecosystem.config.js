'use strict';

module.exports = {
    name : "ghost-api",
	script: 'server.js',
	env: {
		HOST: '127.0.0.1',
		PORT: '3334',
		NODE_ENV: 'development',
		APP_URL: 'http://${HOST}:${PORT}',
		CACHE_VIEWS: false,
		APP_KEY: 'aZlyMswjkx0tICfQNhd3d2PJMODKPadI',
		DB_CONNECTION: 'mysql',
		DB_HOST: '127.0.0.1',
		DB_PORT: '3306',
		DB_USER: 'root',
		SESSION_DRIVER: 'cookie',
		DB_PASSWORD: 'database-user-password',
		DB_DATABASE: 'ghost',
		SERVICE_NAME: 'APP'
	}
};
