'use strict';

const Route = use('Route');
const Database = use('Database');

//upload file to s3
Route.get('/getPreSignedUrl/:filename/image', 'ImageToS3Controller.getPreSignedUrl')
//post from db
Route.get('/post', 'PostController.userPost').middleware([ 'languageHeader' ]);
Route.post('/posts/:id', 'PostController.addNewTranslation').middleware([ 'languageHeader' ]);
//user
Route.group('', function() {
	Route.post('/authentication/token', 'UserController.login').middleware([ 'login' ]);
	//Route.post('/user/setup', 'UserController.signUp');
	Route.get('/users', 'UserController.index').middleware([ 'languageHeader' ]);
	//Route.get('/users/me', 'UserController.showMe').middleware([ 'languageHeader' ]);
	 Route.get('/users/:id', 'UserController.show').middleware([ 'languageHeader' ]);
	// Route.get('/users/slug/:slug', 'UserController.showBySlug').middleware([ 'languageHeader' ]);
	// Route.put('/users/password/', 'UserController.changePassword').middleware([ 'languageHeader' ]);
	// Route.put('/users/:id', 'UserController.put');
	// Route.delete('/users/:id', 'UserController.delete').middleware([ 'languageHeader' ]);
});

//post
Route.group('', function() {
	Route.post('/posts', 'PostController.post').middleware([ 'languageHeader' ]);
	Route.get('/posts', 'PostController.index').middleware([ 'languageHeader' ]);
	Route.get('/posts/:id', 'PostController.show').middleware([ 'languageHeader' ]);
	Route.put('/posts/:id', 'PostController.put').middleware([ 'languageHeader' ]);
	Route.delete('/posts/:id', 'PostController.delete').middleware([ 'languageHeader' ]);
	Route.get('posts/slug/:slug', 'PostController.showBySlug').middleware([ 'languageHeader' ]);
});

Route.get('/url', 'UserController.url').middleware([ 'languageHeader' ]);
//tags
Route.group('', function() {
	Route.post('/tags', 'TagController.post').middleware([ 'languageHeader' ]);
	Route.get('/tags', 'TagController.index').middleware([ 'languageHeader' ]);
	Route.get('/tags/:id', 'TagController.show').middleware([ 'languageHeader' ]);
	Route.put('/tags/:id', 'TagController.put').middleware([ 'languageHeader' ]);
	Route.delete('/tags/:id', 'TagController.delete').middleware([ 'languageHeader' ]);
});

// //roles
// Route.get('/roles', 'RoleController.index');

// //slugs
// Route.get('/slugs/post/:slug', 'SlugController.show');

// //notifications
// Route.get('/notifications', 'NotificationController.index');

// //invites
// Route.group('', function() {
// 	Route.get('/invites', 'InviteController.index');
// 	Route.post('/invites', 'InviteController.post');
// 	Route.get('/invites/:id', 'InviteController.show');
// 	Route.delete('/invites/:id', 'InviteController.delete');
// });

// //setting
// Route.group('', function() {
// 	Route.get('/settings', 'SettingController.index');
// 	Route.put('/settings', 'SettingController.put');
// });

// //configuration
// Route.group('', function() {
// 	Route.get('/configuration', 'ConfigurationController.index');
// 	Route.get('/configuration/about', 'ConfigurationController.about');
// });

// //migration
// Route.group('', function() {
// 	Route.post('/db', 'MigrationController.post');
// 	Route.get('/db', 'MigrationController.index');
// 	Route.delete('/db', 'MigrationController.delete');
// });

// //test mail configuration
// Route.post('/mail/test', 'TestMailConfigController.post');

// //setup
// Route.group('', function() {
// 	Route.post('/authentication/setup', 'SetupController.post');
// 	Route.get('/authentication/setup', 'SetupController.index');
// });
