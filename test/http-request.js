'use strict';

import * as rp from 'request-promise';

let httpRequest = {};
const baseUrl = 'http://127.0.0.1:3333/';

httpRequest.postToken = function(url, data) {
	return rp
		.post({
			url: `${baseUrl}${url}`,
			headers: {
				'Content-Type': 'application/json'
			},
			body: data,
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};

httpRequest.post = function(url, data) {
	return rp
		.post({
			url: `${baseUrl}${url}`,
			headers: {
				Authorization:
				'Bearer U8jiQupFIVCYLSUxwv7nK4BvhuFXIhBy6GaFNxg7UTuk2CIwGyyUTbCfXak3spB7ZSvPkaKOua5Gy6h6O44h3xqEMs6k7IFH2LJHDPgFTfKYOUi2eAkU3pa6AJpH6aXQUhTAgGy4nI9tMjP8sunBnfwcMeL7TOwUuJgrKbJWhPDXyyNwYgrmpczPrY54Hvk',
			'x-language': 'english'
			},
			body: data,
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};

httpRequest.get = function(url, id) {
	if (!id) {
		return rp
			.get({
				url: `${baseUrl}${url}`,
				headers: {
					Authorization:
						'Bearer U8jiQupFIVCYLSUxwv7nK4BvhuFXIhBy6GaFNxg7UTuk2CIwGyyUTbCfXak3spB7ZSvPkaKOua5Gy6h6O44h3xqEMs6k7IFH2LJHDPgFTfKYOUi2eAkU3pa6AJpH6aXQUhTAgGy4nI9tMjP8sunBnfwcMeL7TOwUuJgrKbJWhPDXyyNwYgrmpczPrY54Hvk',
					'x-language': 'english'
				},
				json: true,
				resolveWithFullResponse: true
			})
			.then((response) => response);
	}
	return rp
		.get({
			url: `${baseUrl}${url}/${id}`,
			headers: {
				Authorization:
					'Bearer U8jiQupFIVCYLSUxwv7nK4BvhuFXIhBy6GaFNxg7UTuk2CIwGyyUTbCfXak3spB7ZSvPkaKOua5Gy6h6O44h3xqEMs6k7IFH2LJHDPgFTfKYOUi2eAkU3pa6AJpH6aXQUhTAgGy4nI9tMjP8sunBnfwcMeL7TOwUuJgrKbJWhPDXyyNwYgrmpczPrY54Hvk',
				'x-language': 'english'
			},
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};

httpRequest.put = function(url, id, data) {
	return rp
		.put({
			url: `${baseUrl}${url}/${id}`,
			headers: {
				Authorization:
					'Bearer U8jiQupFIVCYLSUxwv7nK4BvhuFXIhBy6GaFNxg7UTuk2CIwGyyUTbCfXak3spB7ZSvPkaKOua5Gy6h6O44h3xqEMs6k7IFH2LJHDPgFTfKYOUi2eAkU3pa6AJpH6aXQUhTAgGy4nI9tMjP8sunBnfwcMeL7TOwUuJgrKbJWhPDXyyNwYgrmpczPrY54Hvk',
				'x-language': 'english'
			},
			body: data,
			json: true,
			resolveWithFullResponse: true
		})
		.then((response) => response);
};


module.exports = httpRequest;
