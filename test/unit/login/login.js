import httpRequest from '../../http-request';



export function authenticate(data) {
    return httpRequest.postToken('authentication/token',data).then(res => res.body);
}