import * as user from './login';

const englishCreds = {
	username: 'ghostenglish@gmail.com',
	password: 'khalani123',
	language_id: '1'
};

const spanishCreds = {
	username: 'ghostjapanese@gmail.com',
	password: 'khalani123',
	language_id: '2'
};

const japaneseCreds = {
	username: 'ghostspanish@gmail.com',
	password: 'khalani123',
	language_id: '3'
};

describe('This test will check english instance authentication', () => {
	it('make a post request to /authentication/token', async () => {
		const userData = await user.authenticate(englishCreds);
		const items = [ 'access_token', 'refresh_token', 'expires_in', 'token_type', 'language' ];
		expect(userData).toContainKeys(items);
	});
});

describe('This test will check spanish instance authentication', () => {
	it('make a post request to /authentication/token', async () => {
		const userData = await user.authenticate(spanishCreds);
		const items = [ 'access_token', 'refresh_token', 'expires_in', 'token_type', 'language' ];
		expect(userData).toContainKeys(items);
	});
});

describe('This test will check japanese instance authentication', () => {
	it('make a post request to /authentication/token', async () => {
		const userData = await user.authenticate(japaneseCreds);
		const items = [ 'access_token', 'refresh_token', 'expires_in', 'token_type', 'language' ];
		expect(userData).toContainKeys(items);
	});
});
