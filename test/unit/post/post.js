import httpRequest from '../../http-request';

export function create(data) {
	return httpRequest.post('posts', data).then((res) => res);
}

export function index() {
	return httpRequest.get('posts').then((res) => res.body);
}

export function show() {
	return httpRequest.get('posts', '5b1a5bb56121ac12fc35bb7f').then((res) => res);
}

export function put(data) {
	return httpRequest.put('posts', '5b1a5bb56121ac12fc35bb7f', data).then((res) => res);
}
