import * as post from './post';
const postDetails = {
    "posts": [
        {
            "custom_excerpt": "published",
            "featured": true,
            "feature_image": null,
            "codeinjection_foot": "hgjhg",
            "codeinjection_head": "hgjghj",
            "custom_template": null,
            "og_image": null,
            "og_title": null,
            "og_description": null,
            "twitter_image": null,
            "twitter_title": "gjhgjhgj",
            "twitter_description": "fjkdsskfhkfhdjsk",
            "locale": null,
            "meta_description": "bdjashdjahsd"
        }
    ]
};
const updatePostDetails = {
    "posts": [
        {
            "id" : '5b1a5bb56121ac12fc35bb7f',
            "custom_excerpt": "published",
            "featured": true,
            "feature_image": null,
            "codeinjection_foot": "hgjhg",
            "codeinjection_head": "hgjghj",
            "custom_template": null,
            "og_image": null,
            "og_title": null,
            "og_description": null,
            "twitter_image": null,
            "twitter_title": "gjhgjhgj",
            "twitter_description": "fjkdsskfhkfhdjsk",
            "locale": null,
            "meta_description": "bdjashdjahsd"
        }
    ]
}

const items = [ 'id', 'ghost_uuid', 'title', 'created_at', 'updated_at' ];

const postItems = [
	'id',
	'uuid',
	'title',
	'slug',
	'html',
	'feature_image',
	'featured',
	'page',
	'status',
	'locale',
	'visibility',
	'meta_title',
	'meta_description',
	'created_at',
	'created_by',
	'updated_at',
	'updated_by',
	'published_at',
	'published_by',
	'custom_excerpt',
	'codeinjection_head',
	'codeinjection_foot',
	'og_image',
	'og_title',
	'og_description',
	'twitter_image',
	'twitter_title',
	'twitter_description',
	'custom_template',
	'author',
	'primary_author',
	'primary_tag',
	'url',
	'comment_id'
];

const storeResultItems = [
    "id",
    "uuid",
    "title",
    "slug",
    "html",
    "feature_image",
    "featured",
    "page",
    "status",
    "locale",
    "visibility",
    "meta_title",
    "meta_description",
    "created_at",
    "created_by",
    "updated_at",
    "updated_by",
    "published_at",
    "published_by",
    "custom_excerpt",
    "codeinjection_head",
    "codeinjection_foot",
    "og_image",
    "og_title",
    "og_description",
    "twitter_image",
    "twitter_title",
    "twitter_description",
    "custom_template",
    "author",
    "primary_author",
    "primary_tag",
    "url",
    "comment_id"
]

describe('This test will check post api of posts', () => {

	it('make a post request to /posts', async () => {
		const data = await post.create(postDetails);
        expect(data.statusCode).toBe(200);
        expect(data.body.posts[0]).toContainKeys(storeResultItems)
	});
});

describe('This test will check index api of posts', () => {
	it('make a get request to /post', async () => {
		const data = await post.index();
		expect(data[0]).toContainKeys(items);
	});
});

describe('This test will check show api of posts', () => {
	it('make a get request to /post/:id', async () => {
		const data = await post.show();
		expect(data.statusCode).toBe(200);
		expect(data.body['posts'][0]).toContainKeys(postItems);
	});
});

describe('This test will check put api of posts', () => {
	it('make a put request to /posts', async () => {
		const data = await post.put(updatePostDetails);        
        expect(data.statusCode).toBe(200);        
        expect(data.body.posts[0]).toContainKeys(storeResultItems)
        
	});
});

