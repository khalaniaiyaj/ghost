import httpRequest from '../../http-request';

export function create(data) {
	return httpRequest.post('tags', data).then((res) => res);
}

export function index() {
	return httpRequest.get('tags').then((res) => res);
}

export function show() {
	return httpRequest.get('tags', 'by-postman').then((res) => res);
}

export function put(data) {
	return httpRequest.put('tags', '5b1a5be16121ac12fc35bb81', data).then((res) => res);
}
