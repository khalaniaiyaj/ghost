import * as tag from './tag';


const tagDetails = {  
    "tags":[  
       {  
          "name":"i am from test",
          "slug":"i-am-from-test",
          "description": "hello",
         
       }
    ]
 }

const tagResultItems = [
            "id",
            "name",
            "slug",
            "visibility",
            "created_at",
            "updated_at",
            "created_by",
            "updated_by"        
    ]


 
const itemsOfTags = [
    "id",
    "name",
    "slug",
    "description",
    "feature_image",
    "visibility",
    "meta_title",
    "meta_description",
    "created_at",
    "created_by",
    "updated_at",
    "updated_by",
    "parent"
]
const updateTagDetails = {
        "tags":[  
           {   "id": "5b1a5be16121ac12fc35bb81",
              "name":"i am from test",
              "slug":"i-am-from-test",
              "description": "hello",
             
           }
        ]
}

describe('This test will check post api of tags', () => {

	it('make a post request to /tags', async () => {
		const data = await tag.create(tagDetails);
        expect(data.statusCode).toBe(200);
        expect(data.body.tags[0]).toContainKeys(tagResultItems)
	});
});


describe('This test will check index api of tags', () => {
	it('make a get request to /tags', async () => {
        const data = await tag.index();
        expect(data.statusCode).toBe(200);        
		expect(data.body.tags[0]).toContainKeys(itemsOfTags);
	});
});

describe('This test will check show api of tags', () => {
	it('make a get request to /tags/:id', async () => {
        const data = await tag.show();
		expect(data.statusCode).toBe(200);
		expect(data.body.tags[0]).toContainKeys(itemsOfTags);
	});
});

describe('This test will check put api of tags', () => {
	it('make a put request to /tags', async () => {
		const data = await tag.put(updateTagDetails);        
        expect(data.statusCode).toBe(200);        
        expect(data.body.tags[0]).toContainKeys(tagResultItems)
        
	});
});
