import httpRequest from '../../http-request';

export function index() {
	return httpRequest.get('users').then((res) => res.body);
}

export function show() {
	return httpRequest.get('users', '1').then((res) => res.body);
}

export function showMe() {
	return httpRequest.get('users/me').then((res) => res.body);
}
