import * as users from './users';

const items = [
             "id",
            "name",
            "slug",
            "ghost_auth_id",
            "email",
            "profile_image",
            "cover_image",
            "bio",
            "website",
            "location",
            "facebook",
            "twitter",
            "accessibility",
            "status",
            "locale",
            "visibility",
            "meta_title",
            "meta_description",
            "tour",
            "last_seen",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by" 
]
describe('This test will check index api of users', () => {
	it('make a get request to /users', async () => {
		const data = await users.index();
         expect(data.statusCode).toBe(200);
		 expect(data.body.users[0]).toContainKeys(items);
	});
});

describe('This test will check show api of users', () => {
	it('make a get request to /users/:id', async () => {
		const data = await users.show();
         expect(data.statusCode).toBe(200);
		 expect(data.body.users[0]).toContainKeys(items);
	});
});

describe('This test will check show api of current users', () => {
	it('make a get request to /users/me', async () => {
		const data = await users.show();
         expect(data.statusCode).toBe(200);
		 expect(data.body.users[0]).toContainKeys(items);
	});
});
